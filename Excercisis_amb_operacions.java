import java.util.List;
import java.util.Scanner;
// hoal
import javax.sound.midi.SysexMessage;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.SourceDataLine;
import java.util.ArrayList;
import java.util.List;
import java.util.*;
import java.util.Random;
import org.graalvm.compiler.virtual.phases.ea.PartialEscapeBlockState.Final;
import java.text.Normalizer;
import java.text.Normalizer.Form;
public class Excercisis_amb_operacions {
    public static void main(final String[] args) {
        Scanner scan = new Scanner(System.in);
        /*
        //////////////////////////////////////////////////////////////////////////////////////   1
        float patata = 10;
        float preu_amb_descompte =  patata - (patata *15) / 100 ;
        float preu_final = preu_amb_descompte + (preu_amb_descompte *21) / 100;
        System.out.println(preu_final);
        
        
        Scanner scan = new Scanner(System.in);

        /////////////////////////////////////////////////////////////////////////////////////    2
        float Costat_rectangle1;
        float Costat_rectangle2;
        float Hipotensua_al_quadrat;

        float radi;

        System.out.println("Costat 1 rectangle");
        Costat_rectangle1 = scan.nextInt();
        System.out.println("Costat 2 rectangle");
        Costat_rectangle2 = scan.nextInt();
        Hipotensua_al_quadrat = (Costat_rectangle1 * Costat_rectangle1) + (Costat_rectangle2 * Costat_rectangle2);
        System.out.println("La hipotenusa del rectangle és " + Math.sqrt(Hipotensua_al_quadrat));
        
        
        System.out.println("Radi cercle");
        radi = scan.nextInt();
        System.out.println("L'area del cercle és " + Math.PI * (radi * radi));
             
        /////////////////////////////////////////////////////////////////////////////////////////////////////////        3
        
        System.out.println("Estic fins als @!\\?\"#€~ del coronavirus");
        
        System.out.println("Estic fins als @!\\?\"#€~ del\n\t coronavirus");
        

        ////////////////////////////////////////////////////////////////////////////////////////////////////////        4
        char lletra = 'c';
        System.out.println("És lletra? " + Character.isLetter(lletra));
        System.out.println("És dígit? " + Character.isDigit(lletra));
        System.out.println("És majúscula? " + Character.isUpperCase(lletra));
        System.out.println("És minúscula? " + Character.isLowerCase(lletra));
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////          5
        System.out.printf("%d",53); 
        System.out.printf("|%20d|", 53); 
        System.out.printf("|%-20d|", 53); 
        System.out.printf("|%020d|", 53); 
        System.out.printf("|%,d|", 10000000); 
        System.out.printf("%.2f", 12.9f);
        System.out.printf("%,.2f", 54999.99789f);
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////            6

        System.out.printf("%.2f\n", 564200.5947);    
        System.out.printf("%,.3f\n", 564200.5947);   
        System.out.printf("%08.0f\n", 564200.5947);  
        System.out.printf("%+011.1f\n    ", 564200.5947);

        //////////////////////////////////////////////////////////////////////////////////////              7
        
        System.out.printf("%-10s%s\n", "ANGLÈS","CATALÀ");
        System.out.printf("%-10s%s\n","--------", "--------");
        System.out.printf("%-10s%s\n","man", "home");
        System.out.printf("%-10s%s\n","woman", "dona");
        System.out.printf("%-10s%s\n","apple", "poma");
        System.out.printf("%-10s%s\n","home", "casa");
        System.out.printf("%-10s%s\n","laptop", "portátil");
        System.out.printf("%-10s%s\n","glass", "vidre");
        System.out.printf("%-10s%s\n","plastic", "plastic");
        System.out.printf("%-10s%s\n","blue", "blau");
        System.out.printf("%-10s%s\n","screen", "pantalla");
        System.out.printf("%-10s%s\n","potato", "patata");
        
        ///////////////////////////////////////////////////////////////////////////////////                 8

        int opcio;

        System.out.println("Tria una opció: '1' '2' '3' '4' '5'");
        opcio = scan.nextInt();
        scan.nextLine();
        System.out.println("Has escollit la opció " + opcio);
        

        //////////////////////////////////////////////////////////////////////////////////                  9

        final String LLETRES = "TRWAGMYFPDXBNJZSQVHLCKE";   //inicialitzem les variables
        int dni;
        int resiud;   
        char lletra;                                        

        System.out.println("Escriu el teu DNI ");           // li demanem a l'usuari que escrigui el seu dni
        dni = scan.nextInt();                               //el guardem a la variable dni
        scan.nextLine();
        
        residu = dni % 23;                              // Calculem el residu necesari per escollir la lletra
        lletra = LLETRES.charAt(residu);               // Agafem de la variable LLETRES la lletra que estigui en la posicio "residu"
        
        System.out.println("La lletra del DNI " + dni + " és " + lletra);
        
        /////////////////////////////////////////////////////////////////////////////////                   10

        float base;
        float altura;                                      //inicialitzem les variables
        float hipotenusa;

        System.out.println("Base triangle ");               // li demanem a l'usuari la base del triangle
        base = scan.nextInt();                              // guardem la dada que a escrit l'usuari a la variable base
        scan.nextLine();
        System.out.println("Altur triangle ");              // li demanem a l'usuari la altura del triangle
        altura = scan.nextInt();                            // guardem la dada que a escrit l'usuari a la variable altura
        scan.nextLine();        
        
        hipotenusa = (float)(Math.sqrt(Math.pow(base, 2) + Math.pow(altura, 2)));        // calculem la hipotenusa i li posem que sigui float, ja que al fer el calcul és posa en double automaticament

        System.out.printf("La Àrea del triangle és %,.3f\n" ,(base * altura / 2));         // Enseñem l'àrea per pantalla amb el . de milers i 6 decimals
        
        System.out.printf("El perímetre del triangle és %,.3f", (base + altura + hipotenusa));     // Enseñem el perímetre per pantalla amb el . de milers i 6 decimals

        /////////////////////////////////////////////////////////////////////////////////////////////       11
        
        double radi;

        System.out.println("Radi cercle ");               // li demanem a l'usuari el radi del cercle
        radi = scan.nextInt();
        scan.nextLine();

        System.out.printf("La Àrea del cercle és %,.6f\n", (Math.PI * Math.pow(radi, 2)));  // Enseñem l'àrea per pantalla amb el . de milers i 6 decimals

        System.out.printf("El perímetre del cercle és %,.6f\n", (2 * (Math.PI * radi)));    // Enseñem el perímetre per pantalla amb el . de milers i 6 decimals
        
        ////////////////////////////////////////////////////////////////////////////////////////////////        12

        int edat;

        System.out.println("Edat de l'usuari ");
        edat = scan.nextInt();
        scan.nextLine();

        System.out.println("Pot conduir cotxe (edat més gran o igual que 18 i inferior a 80) " + (boolean)(edat >= 18 && edat < 80));
        System.out.println("No està en edat de treballar (es pot treballar ente els 16 i els 65 anys, tots dos inclosos) " + (boolean)!(edat >= 16 && edat <= 65));
        System.out.println("Ha d'anar a la guarderia (té menys de 3 anys) " + (boolean)(edat < 3));
        System.out.println("Pot estudiar un CFGS (edat entre 18 i 24, totes dues incloses) " + (boolean)(edat >= 18 && edat <= 24));
        
        ////////////////////////////////////////////////////////////////////////////////////////////////        13
        
        final float P1  = 80 , P2 = 45 , P3 = 23, P4 = 30, P5 = 30, P6 = 23;
        float nota1, Mitjana;
        float nota2;
        float nota3;
        float nota4;
        float nota5;
        float nota6;

        System.out.println("Posa les notes 6 notes separades per espais: ");
        nota1 = scan.nextFloat();
        nota2 = scan.nextFloat();
        nota3 = scan.nextFloat();
        nota4 = scan.nextFloat();
        nota5 = scan.nextFloat();
        nota6 = scan.nextFloat();
        scan.nextLine();

        Mitjana = (nota1 * P1 + nota2 * P2 + nota3 * P3 + nota4 * P4 + nota5 * P5 + nota6 * P6) / 231 ; 

        System.out.printf("La nota final de M3 és %.1f" , Mitjana);
        
        //////////////////////////////////////////////////////////////////////////////////////////////      14

        float segons, minuts, hores;

        System.out.println("Segons(maxim 360000): ");
        segons = scan.nextInt();
        scan.nextLine();

        minuts = segons / 60;

        segons = segons % 60;

        hores = minuts / 60 ;

        minuts = minuts % 60;

        System.out.printf("%02.0f:%02.0f:%02.0f ", hores, minuts, segons);
        
        /////////////////////////////////////////////////////////////////////////////////////   15

        float capacitat, consum;
        int kilometres, metres;

        System.out.println("Capacitat maxima diposit: ");
        capacitat = scan.nextFloat();
        scan.nextLine();

        System.out.println("Consum per cada 100Km: ");
        consum = scan.nextFloat();
        scan.nextLine();

        kilometres = (int)(capacitat * 100 / consum);

        metres  = (int)(((capacitat * 100 / consum) * 1000) % 1000);
        

        System.out.printf("Pot circular durant %d KM i %d metres ", kilometres, metres);
        
        //////////////////////////////////////////////////////////////////////////////////////          16

        float preu_orig, increment, preu_pre_BF, descompte, preu_BF, descompte_real = 1;
        String nom_article;


        System.out.println("Nom de l'article: ");
        nom_article = scan.nextLine();
        
        System.out.println("Preu Original: ");
        preu_orig = scan.nextFloat();
        scan.nextLine();

        System.out.println("% incrementat pre BF: ");
        increment = scan.nextFloat();
        scan.nextLine();

        System.out.println("Descompte BF: ");
        descompte = scan.nextFloat();
        scan.nextLine();


        preu_pre_BF = (preu_orig * increment / 100) + preu_orig;
        preu_BF = preu_pre_BF - (preu_pre_BF * descompte / 100) ;
        descompte_real = 100 - (preu_BF * 100 / preu_orig);

        System.out.printf("%-20s%17s%17s%17s%17s%17s\n", "Nom del producte", "Preu orig", "Preu pre BF", "Dte BlackFr", "Preu BlackFr", "Dte real");

        System.out.printf("%-20s%15.2f €%15.2f €%15.2f %%%15.2f €%15.2f %%", nom_article, preu_orig, preu_pre_BF, descompte, preu_BF, descompte_real);
        
        //////////////////////////////////////////////////////////////////////////      17
        
        float import_prestec, interes_anual, import_interesos, import_mensual;
        int num_placos;


        System.out.println("Import del préstec: ");
        import_prestec = scan.nextFloat();
        scan.nextLine();

        System.out.println("Interès anual: ");
        interes_anual = scan.nextFloat();
        scan.nextLine();

        System.out.println("Nombre de plaços: ");
        num_placos = scan.nextInt();
        scan.nextLine();
        
        import_interesos = import_prestec * interes_anual / 100;

        import_mensual = (import_prestec + import_interesos) / 10;


        System.out.printf("%-27s%10.2f €\n","Import del préstec", import_prestec);
        System.out.printf("%-27s%10.2f %%\n","Interès anual", interes_anual);
        System.out.printf("%-27s%7d plçs\n","Nombre de plaços", num_placos);
        System.out.printf("%-27s%10.2f €\n","Import dels interessos", import_interesos);
        System.out.printf("%-27s%10.2f €\n","Import a pagar mensualment", import_mensual);
        
        ////////////////////////////////////////////////////////////////////////////////////        18
        
        float preu, pagat, canvi;
        int canviC, dos€ = 0, un€ = 0, cinquantac = 0, vintc = 0, deuc = 0;

        
        System.out.println("Preu: ");
        preu = scan.nextFloat();
        scan.nextLine();

        System.out.println("Quan has pagat? ");
        pagat = scan.nextFloat();
        scan.nextLine();

        canvi  = pagat - preu;
        canviC = Math.round(canvi * 100);
        
        dos€ = canviC / 200;
        canviC = canviC % 200;
        un€ = canviC / 100;
        canviC = canviC % 100;
        cinquantac = canviC / 50;
        canviC = canviC % 50;
        vintc = canviC / 20;
        canviC = canviC % 20;
        deuc = canviC / 10;
        canviC = canviC % 10;

        System.out.printf("El canvi seran %.2f €:\n", canvi);
        System.out.printf("%d monedes de 2 euros\n", dos€);
        System.out.printf("%d monedes de 1 euros\n", un€);
        System.out.printf("%d monedes de 50 centims\n", cinquantac);
        System.out.printf("%d monedes de 20 centims\n", vintc);
        System.out.printf("%d monedes de 10 centims\n", deuc);
        
        ///////////////////////////////////////////////////////////////////         19
        
        float ingresos_fi_mes, despeses_mensuals, total;

        System.out.println("Quants diners guanyes al més? ");
        ingresos_fi_mes = scan.nextFloat();
        scan.nextLine();

        System.out.println("Quants diners has gastat aquest més? ");
        despeses_mensuals = scan.nextFloat();
        scan.nextLine();

        total = ingresos_fi_mes - despeses_mensuals;

        
        System.out.printf("Dels %.2f € qué has guanyat aquest més et quedan %.2f €\n", ingresos_fi_mes, total);
                     
        
        //////////////////////////////////////////////////////////////////  20

        int mes;

        System.out.println("Introdueix un número del mes: ");
        mes = scan.nextInt();
        scan.nextLine();

        if (mes == 1){
            System.out.println("Gener té 31 dies ");
        }
        else if (mes == 2){
            int any;

            System.out.println("Introdueix l'any: ");
            any = scan.nextInt();
            scan.nextLine();

            if (any % 400 == 0){
                System.out.println("Febrer té 29 dies");
            }
            else if (any % 4 == 0){
                if (any % 100 == 0){
                    System.out.println("Febrer té 28 dies");
                }
                else{
                    System.out.println("Febrer té 29 dies");
                }
            }
            else{
                System.out.println("Febrer té 28 dies");
            }
        }
        else if (mes == 3){
            System.out.println("Març té 31 dies ");
        }
        else if (mes == 4){
            System.out.println("Abril té 30 dies ");
        }
        else if (mes == 5){
            System.out.println("Maig té 31 dies ");
        }
        else if (mes == 6){
            System.out.println("Juny té 30 dies ");
        }
        else if (mes == 7){
            System.out.println("Juliol té 31 dies ");
        }
        else if (mes == 8){
            System.out.println("Agost té 31 dies ");
        }
        else if (mes == 9){
            System.out.println("Setembre té 30 dies ");
        }
        else if (mes == 10){
            System.out.println("Octubre té 31 dies ");
        }
        else if (mes == 11){
            System.out.println("Novembre té 30 dies ");
        }
        else if (mes == 12){
            System.out.println("Desembre té 31 dies ");
        }
        else{
            System.out.println("No has introduit un numero entre 1 i 12 ");
        }

        
        //////////////////////////////////////////////////////////////////  21

        
        float valor1, valor2, resultat = 0;
        char caracter;
        boolean operacio_invalida = false;

        System.out.println("Escriu la operacio (valor operació (+,-,* o /) valor): ");
        valor1 = scan.nextFloat();
        caracter = scan.next().charAt(0);
        valor2 = scan.nextFloat();
        scan.nextLine();

        switch(caracter){
            case '+':
                resultat = valor1 + valor2;
                break;
            case '-':
                resultat = valor1 - valor2;
                break;
            case '*':
                resultat = valor1 * valor2;
                break;
            case '/':
                resultat = valor1 / valor2;
                break;
        }

        System.out.println("El resultat és " + resultat);
        
        ///////////////////////////////////////////////////////////////     22

        float preu;

        System.out.println("Introdueix el preu del producte ");
        preu = scan.nextFloat();
        scan.nextLine();

        if (preu >= 500){
            preu = preu - (preu * 10 / 100);
        }
        
        if (preu < 20){
            preu += 5;
        }

        System.out.printf("El preu final són %.2f €", preu);
        
        ////////////////////////////////////////////////////////////        23

        final String CONTRASENYA = "123456";
        String contrasenya;

        System.out.println("Introdueix la contrasenya ");
        contrasenya = scan.nextLine();

        if (contrasenya.equals(CONTRASENYA)){
            System.out.println("Contrasenya correcte ");
        }
        else{
            System.out.println("Contrasenya incorrecte ");
        }
        
        ///////////////////////////////////////////////////////////         24

        int num1, num2;

        System.out.println("Introdueix els 2 numeros ");
        num1 = scan.nextInt();
        num2 = scan.nextInt();
        scan.nextLine();

        if (num1 % num2 == 0){
            System.out.printf("%d és múltiple de %d ", num1, num2);
        }
        else{
            System.out.printf("%d no és múltiple de %d ", num1, num2);
        }
        
        ////////////////////////////////////////////////////////        25

        int num;

        System.out.println("Introdueix el numero ");
        num = scan.nextInt();
        scan.nextLine();

        if (num > 0){
            System.out.printf("%d és positiu ", num);
        }
        else if (num == 0){
            System.out.printf("%d és igual a 0 ", num);
        }
        else{
            System.out.printf("%d és negatiu ", num);
        }
        
        ////////////////////////////////////////////////////////////////////     26
        
        int edat;
        

        System.out.println("Quina és la teva edat? ");
        edat = scan.nextInt();
        scan.nextLine();

        if (edat >= 18 && edat < 80){
            System.out.println("\"Pots conduir un cotxe\" ");
        }
        else{
            System.out.println("\"No pots conduir un cotxe\" ");
        }
        if(edat < 16 ||  edat > 65){
            System.out.println("\"No estàs en edat de treballar\"");
        }
        else{
            System.out.println("\"Estàs en edat de treballar\"");
        }
        if(edat < 3){
            System.out.println("\"Has d'anar a la guarderia\"");
        }
        else{
            System.out.println("\"No has d'anar a la guarderia\"");     
        }
        if(edat >= 18 && edat <= 24){
            System.out.println("\"Pots estudiar un CFGS\"");
        }
        else{
            System.out.println("\"No pots estudiar un CFGS\"");       
        }
        
        ////////////////////////////////////////////////////////////    27

        int num1, num2, num3;

        System.out.println("Introdueix els 3 numeros ");
        num1 = scan.nextInt();
        num2 = scan.nextInt();
        num3 = scan.nextInt();
        scan.nextLine();
        
        if (num1 > num2 && num1 > num3){
            System.out.println(num1 + " és el numero més gran ");
        }
        else if (num2 > num1 && num2 > num3){
            System.out.println(num2 + " és el numero més gran ");
        }
        else if (num3 > num1 && num3 > num1){
            System.out.println(num3 + " és el numero més gran ");
        }
        else{
            System.out.println("No hi ha numero més gran ");
        }
        
        ///////////////////////////////////////////////////////////////     28
        
        float preu, pagat, canvi;
        int canviC, dos€ = 0, un€ = 0, cinquantac = 0, vintc = 0, deuc = 0;

        
        System.out.println("Preu: ");
        preu = scan.nextFloat();
        scan.nextLine();

        System.out.println("Quan has pagat? ");
        pagat = scan.nextFloat();
        scan.nextLine();

        canvi  = pagat - preu;
        canviC = Math.round(canvi * 100);
        
        dos€ = canviC / 200;
        canviC = canviC % 200;
        un€ = canviC / 100;
        canviC = canviC % 100;
        cinquantac = canviC / 50;
        canviC = canviC % 50;
        vintc = canviC / 20;
        canviC = canviC % 20;
        deuc = canviC / 10;
        canviC = canviC % 10;

        System.out.printf("El canvi seran %.2f €:\n", canvi);
        if (dos€ != 0){
            if (dos€ > 1){
                System.out.printf("%d monedes de 2 euros\n", dos€);
            }
            else{
                System.out.printf("%d moneda de 2 euros\n", dos€);
            }
            
        }
        if (un€ != 0){
            if (un€ > 1){
                System.out.printf("%d monedes de 1 euro\n", un€);
            }
            else{
                System.out.printf("%d moneda de 1 euro\n", un€);
            }
            
        }
        if (cinquantac != 0){
            if (cinquantac > 1){
                System.out.printf("%d monedes de 50 centims\n", cinquantac);
            }
            else{
                System.out.printf("%d moneda de 50 centims\n", cinquantac);       
            }
            
        }
        if (vintc != 0){
            if (vintc > 1){
                System.out.printf("%d monedes de 20 centims\n", vintc);
            }
            else{
                System.out.printf("%d moneda de 20 centims\n", vintc);              
            }
            
        }
        if (deuc != 0){
            if (deuc > 1){
                System.out.printf("%d monedes de 10 centims\n", deuc);
            }
            else{
                System.out.printf("%d moneda de 10 centims\n", deuc);            
            }
            
        }
        
        /////////////////////////////////////////////////////////////////   29
        
        int any;

        System.out.println("Any: ");
        any = scan.nextInt();
        scan.nextLine();

        if (any % 400 == 0){
            System.out.println("Sí");
        }
        else if (any % 4 == 0){
            if (any % 100 == 0){
                System.out.println("No");
            }
            else{
                System.out.println("Sí");
            }
        }
        else{
            System.out.println("No");
        }
        
         //////////////////////////////////////////////////////////  30

         float valor1, valor2, resultat = 0;
         char caracter;
         boolean operacio_invalida = false;
 
         System.out.println("Introdueix el primer valor: ");
         valor1 = scan.nextFloat();
         scan.nextLine();
 
         System.out.println("Introdueix el segon valor: ");
         valor2 = scan.nextFloat();
         scan.nextLine();
 
         System.out.println("Quina operació vols fer (+, -, * o /)?  ");
         caracter = scan.next().charAt(0);
         scan.nextLine();
 
         if (caracter == '+'){
             resultat = valor1 + valor2;
         }
         else if(caracter == '-'){
             resultat = valor1 - valor2;
         }
         else if(caracter == '*'){
             resultat = valor1 * valor2;
         }
         else if(caracter == '/'){
             resultat = valor1 / valor2;
         }
         else{
             System.out.println("Introdueix una operació valida ");
             operacio_invalida = true;
         }
 
         if (operacio_invalida == false){
             System.out.println("EL resultat és " + resultat);
         }
         
        /////////////////////////////////////////////////////////////   31

         int valor1, valor2;

         System.out.println("Introdueix 2 valors: ");
         valor1 = scan.nextInt();
         valor2 = scan.nextInt();
         scan.nextLine();

        valor1 += valor2;

        valor2 = valor1 - valor2;

        valor1 = valor1 - valor2;

         System.out.println(valor1 + " " + valor2);
         
        //////////////////////////////////////////////////////////  32
         
        int numero;

        System.out.println("Introdueix un numero del 1 al 7 ");
        numero = scan.nextInt();
        scan.nextLine();

        switch (numero){
            case 1:
                System.out.println("Avui és dilluns ");
                break;
            case 2:
                System.out.println("Avui és dimarts ");
                break;
            case 3:
                System.out.println("Avui és dimecres ");
                break;
            case 4:
                System.out.println("Avui és dijous ");
                break;
            case 5:
                System.out.println("Avui és divendres ");
                break;
            case 6:
                System.out.println("Avui és dissabte ");
                break;
            case 7:
                System.out.println("Avui és diumenge ");
                break;
            default:
                System.out.println("No has introduit un valor entre 1 i 7 ");
                
        }
        
        ///////////////////////////////////////////////////     33

        int nota;

        System.out.println("Nota ");
        nota = scan.nextInt();
        scan.nextLine();

        if (nota < 0){
            System.out.println("La nota és inferior a 0 ");
        }
        else if (nota > 10){
            System.out.println("La nota és superior a 10 ");
        }
        else if(nota < 5){
            System.out.println("Suspes ");
        }
        else{
            System.out.println("Aprobat ");
        }
        // if (nota < 0 || nota > 10){
        //     if (nota < 0){
        //         System.out.println("La nota és inferior a 0 ");
        //     }
        //     else{
        //         System.out.println("La nota és superior a 10 ");
        //     }
        // }
        // else{
        //     if (nota < 5){
        //         System.out.println("Suspes ");
        //     }
        //     else{
        //         System.out.println("Aprobat ");
        //     }
        // }
        
        ///////////////////////////////////////////////////////////////////////////     34

        int numero;

        System.out.println("Introdueix un numero del 1 al 7 ");
        numero = scan.nextInt();
        scan.nextLine();

        if (numero == 1){
            System.out.println("Dilluns ");
        }
        else if (numero == 2){
            System.out.println("DImarts ");
        }
        else if (numero == 3){
            System.out.println("Dimecres ");
        }
        else if (numero == 4){
            System.out.println("Dijous ");
        }
        else if (numero == 5){
            System.out.println("DIvendres ");
        }
        else if (numero == 6){
            System.out.println("Dissabte ");
        }
        else if (numero == 7){
            System.out.println("Diumenge ");
        }
        else{
            System.out.println("El numero no esta entre 1 i 7 ");
        }
        
        ///////////////////////////////////////////////////////////////////////     35

        float temperatura, humitat;

        System.out.println("Temperatura ");
        temperatura = scan.nextFloat();
        scan.nextLine();

        System.out.println("Humitat ");
        humitat = scan.nextFloat();
        scan.nextLine();

        if ((temperatura > 25 || temperatura < 5) || (humitat > 80 || humitat < 40)){
            System.out.println("Error ");
            // byte[] buf = new byte[ 1 ];;
            // AudioFormat af = new AudioFormat( (float )44100, 8, 1, true, false );
            // SourceDataLine sdl = AudioSystem.getSourceDataLine( af );
            // sdl.open();
            // sdl.start();
            // for( int i = 0; i < 1000 * (float )44100 / 1000; i++ ) {
            //     double angle = i / ( (float )44100 / 440 ) * 2.0 * Math.PI;
            //     buf[ 0 ] = (byte )( Math.sin( angle ) * 100 );
            //     sdl.write( buf, 0, 1 );
            // }
            // sdl.drain();
            // sdl.stop();
        }
        else{
            System.out.println("Ningun error ");
        }
        
        /////////////////////////////////////////////////////////////   36

        float nota;

        System.out.println("Introdueix una nota ");
        nota = scan.nextInt();
        scan.nextLine();

        if (nota < 0){
            System.out.println("Error ");
        }
        else if (nota < 3){
            System.out.println("Has de millorar moooolt! ");
        }
        else if (nota >= 3 && nota < 5){
            System.out.println("Encara et falta una mica per aprovar ");
        }
        else if (nota >= 5 && nota < 7){
            System.out.println("No vas malament però segur que pots millorar una mica ");
        }
        else if ( nota >= 7 && nota < 9){
            System.out.println("Força bé ");
        }
        else if ( nota >= 9 && nota <= 10){
            System.out.println("Excel·lent! ");
        }
        else{
            System.out.println("Error ");
        }
        
        /////////////////////////////////////////////////////////////////////////////   37
        
        int num1, num2, num3;

        System.out.println("Introdueix els 3 numeros ");
        num1 = scan.nextInt();
        num2 = scan.nextInt();
        num3 = scan.nextInt();
        scan.nextLine();
        
        if (num1 >= num2 && num1 >= num3){
            
            if (num2 > num3){
                System.out.println(num1 + " "  + num2 + " "  + num3);
            }
            else if(num2 == num3){
                System.out.println(num1 + " "  + num2 + " "  + num3);
            }
            else{
                System.out.println(num1 + " "  + num3 + " "  + num2);
            }
        }
        else if (num2 > num1 && num2 >= num3){
            if (num1 > num3){
                System.out.println(num2 + " "  + num1 + " "  + num3);
            }
            else if(num1 == num3){
                System.out.println(num2 + " "  + num1 + " "  + num3);
            }
            else{
                System.out.println(num2 + " "  + num3 + " "  + num1);
            }
        }
        else if (num3 > num1 && num3 > num2){
            if (num2 > num1){
                System.out.println(num3 + " "  + num2 + " "  + num1);
            }
            else if(num2 == num1){
                System.out.println(num3 + " "  + num1 + " "  + num2);
            }
            else{
                System.out.println(num3 + " "  + num1 + " "  + num2);
            }
        }
        else{
            System.out.println("Error");
        }
        
        /////////////////////////////////////////////////////////////////////////////   38

        int mes;

        System.out.println("Introdueix un número del mes: ");
        mes = scan.nextInt();
        scan.nextLine();

        if (mes == 1){
            System.out.println("Gener té 31 dies ");
        }
        else if (mes == 2){
            int any;

            System.out.println("Introdueix l'any: ");
            any = scan.nextInt();
            scan.nextLine();

            if (any % 400 == 0){
                System.out.println("Febrer té 29 dies");
            }
            else if (any % 4 == 0){
                if (any % 100 == 0){
                    System.out.println("Febrer té 28 dies");
                }
                else{
                    System.out.println("Febrer té 29 dies");
                }
            }
            else{
                System.out.println("Febrer té 28 dies");
            }
        }
        else if (mes == 3){
            System.out.println("Març té 31 dies ");
        }
        else if (mes == 4){
            System.out.println("Abril té 30 dies ");
        }
        else if (mes == 5){
            System.out.println("Maig té 31 dies ");
        }
        else if (mes == 6){
            System.out.println("Juny té 30 dies ");
        }
        else if (mes == 7){
            System.out.println("Juliol té 31 dies ");
        }
        else if (mes == 8){
            System.out.println("Agost té 31 dies ");
        }
        else if (mes == 9){
            System.out.println("Setembre té 30 dies ");
        }
        else if (mes == 10){
            System.out.println("Octubre té 31 dies ");
        }
        else if (mes == 11){
            System.out.println("Novembre té 30 dies ");
        }
        else if (mes == 12){
            System.out.println("Desembre té 31 dies ");
        }
        else{
            System.out.println("No has introduit un numero entre 1 i 12 ");
        }
        
        /////////////////////////////////////////////////////////////////////////////   39

        int prestatgeries, nombre_prestatges, numero_llibres_per_prestatge, llibres_nous, llibres_que_entren, prestatgeries_sobrants, prestatgeries_necessaries;

        System.out.println("Nombre prestatgeries: ");
        prestatgeries = scan.nextInt();
        scan.nextLine();

        System.out.println("Nombre prestatges: ");
        nombre_prestatges = scan.nextInt();
        scan.nextLine();

        System.out.println("Numero de llibres per prestatge: ");
        numero_llibres_per_prestatge = scan.nextInt();
        scan.nextLine();

        System.out.println("Llibres nous: ");
        llibres_nous = scan.nextInt();
        scan.nextLine();      
        
        llibres_que_entren = (prestatgeries * nombre_prestatges) * numero_llibres_per_prestatge;
        prestatgeries_necessaries = llibres_nous / (nombre_prestatges * numero_llibres_per_prestatge);

        if (llibres_nous % (nombre_prestatges * numero_llibres_per_prestatge) != 0){
            ++prestatgeries_necessaries;
        }

        if (prestatgeries <= 0 || nombre_prestatges <= 0 || numero_llibres_per_prestatge <= 0 || llibres_nous <= 0){
            System.out.println("Algun valor introduit és 0 o més petit que 0 ");
        }
        else if (llibres_que_entren >= llibres_nous){
            System.out.println("Entren ");
            
            prestatgeries_sobrants =  prestatgeries - prestatgeries_necessaries;
            System.out.println("Sobren " + prestatgeries_sobrants + " prestatgeries");
        }
        else{
            System.out.println("No entren ");

            prestatgeries_sobrants =  prestatgeries_necessaries - prestatgeries;
            System.out.println("Falten " + prestatgeries_sobrants + " prestatgeries");
        }
        
        /////////////////////////////////////////////////////////////////////////////   40

        float temperatura;
        String off_on;
        

        System.out.println("Temperatura: ");
        temperatura = scan.nextFloat();
        scan.nextLine();

        // String off_on = (temperatura < 20) ? "on" : "off";
        if (temperatura < 20){
            off_on = "on";
        }
        else{
            off_on = "off";
        }

        if (off_on.equals("off")){
            System.out.println("Calefacció apagada ");
        }
        else{
            System.out.println("Calefacció encesa ");
        }
        
        /////////////////////////////////////////////////////////////////////////////   41

        float temperatura;
        

        System.out.println("Temperatura: ");
        temperatura = scan.nextFloat();
        scan.nextLine();

        int interruptor = (temperatura < 20) ? -1 : (temperatura > 24) ? 1 : 0;

        if (interruptor == -1){
            System.out.println("Calefacció encesa ");
        }
        else if(interruptor == 1){
            System.out.println("Aire condicionat encès ");
        }
        else{
            System.out.println("Què bé! Estem estalviant energia! ");
        }
        
        /////////////////////////////////////////////////////////////////////////////   42
        
        String mes;

        System.out.println("Escriu en quin mes estem ");
        mes = scan.nextLine();
        mes = mes.toLowerCase();


        switch (mes){
            case "febrer" : 
                int any;

                System.out.println("Introdueix l'any: ");
                any = scan.nextInt();
                scan.nextLine();

                if (any % 400 == 0 || (any % 4 == 0 && any % 100 != 0)){
                    System.out.println("Febrer té 29 dies");
                }
                else{
                    System.out.println("Febrer té 28 dies");
                }
                break;
            case "abril" : case "juny" : case "setembre" : case "novembre" :
                System.out.println("El mes te 30 dies");
                break;
            case "gener" : case "marzo" : case "maig" : case "juliol" : case "agost" : case "octubre" : case "desembre" :
                System.out.println("El mes te 31 dies");
                break;
            default :
                System.out.println("No has escrit el nom del mes correctament");
               
        }
        
        /////////////////////////////////////////////////////////////////////////////   43


        char lletra;

        System.out.println("Quina nota té (A, B, C, D)?  ");
        lletra = scan.next().charAt(0);
        scan.nextLine();
        lletra = Character.toUpperCase(lletra);

        switch (lletra){
            case 'A':
                System.out.println("A=Excel·lent (9-10)");
                break;
            case 'B':
                System.out.println("B=Notable (7-8)");
                break;
            case 'C':
                System.out.println("C=Aprovat (5-6)");
                break;
            case 'D':
                System.out.println("D=Suspès (1-4)");  
                break;
            default:
                System.out.println("No has introduit una de les lletres possibles"); 
        }

        
        /////////////////////////////////////////////////////////////////////////////   44

        int nota;

        System.out.println("Quina nota té ?  ");
        nota = scan.nextInt();
        scan.nextLine();

        switch (nota){
            case 9: case 10:
                System.out.println("9-10=A");
                break;
            case 7: case 8:
                System.out.println("7-8=B");
                break;
            case 6: case 5:
                System.out.println("5-6=C");
                break;
            case 4: case 3: case 2: case 1:
                System.out.println("1-4=Suspès");  
                break;
            default:
                System.out.println("No has introduit una nota entre 1 i 10"); 
        }

        
        /////////////////////////////////////////////////////////////////////////////   45

        float temperatura, presio_max;
        String tos;
        char categoria;

        
        System.out.println("Quina temperatura té ?  ");
        temperatura = scan.nextFloat();
        scan.nextLine();
                
        System.out.println("Quina tos té (No, Seca, Expectorant)?  ");
        tos = scan.nextLine();

        System.out.println("Quina presio maxima té ?  ");
        presio_max = scan.nextFloat();
        scan.nextLine();
        
        if (temperatura < 25 || temperatura > 45){
            if (temperatura < 25){
                System.out.println("La temperatura esta per sota de 25");
            }
            else{
                System.out.println("La temperatura esta per sobre de 45");
            }
            return;
        }
        else if(!(tos.equals("No") || tos.equals("Seca") || tos.equals("Expectorant"))){
            System.out.println("No has posat una de les opcions");
            return;
        }
        else if(presio_max < 80 || presio_max > 180){
            if (presio_max < 80){
                System.out.println("La presio maxima esta per sota de 80");
            }
            else{
                System.out.println("La presio maxima esta per sobre de 180");
            }
            return;
        }


        if (temperatura < 32 || temperatura > 42 || presio_max < 100){
            categoria = 'M';
        }
        else if ((temperatura >= 35 && temperatura <= 38) && tos.equals("No") && (presio_max >= 120 && presio_max <=140)){
            categoria = 'E';
        }
        else if (temperatura > 38 && tos.equals("Expectorant") ){
            categoria = 'D';
        }
        else if (temperatura > 38 && tos.equals("Seca")){
            categoria = 'C';
        }
        else if ((temperatura >= 35 && temperatura <= 38) && tos.equals("No") && (presio_max < 120 || presio_max > 140)){
            categoria = 'B';
        }
        else{
            categoria = 'A';
        }

        switch (categoria){
            case 'M':
                System.out.println("No hi ha res a fer.");
                
            case 'E':
                System.out.println("Que esperi sentat... o estirat.");
                break;
            case 'D':
                System.out.println("L'ha de visitar el neumòleg d'urgències.");
                break;
            case 'C':
                System.out.println("Envia'l a la planta COVID.");
                break;
            case 'B':
                System.out.println("Ràpid! Que vingui un cardiòleg!");
                break;
            default:
                System.out.println("Fes-lo esperar una mica i li passes al Dr House, el de 'casos especials'.");
                
        }

        
        /////////////////////////////////////////////////////////////////////////////   46

        float temperatura, presio_max;
        String tos;
        char categoria;

        
        System.out.println("Quina temperatura té ?  ");
        temperatura = scan.nextFloat();
        scan.nextLine();
                
        System.out.println("Quina tos té (No, Seca, Expectorant)?  ");
        tos = scan.nextLine();

        System.out.println("Quina presio maxima té ?  ");
        presio_max = scan.nextFloat();
        scan.nextLine();
        
        if (temperatura < 25 || temperatura > 45){
            if (temperatura < 25){
                System.out.println("La temperatura esta per sota de 25");
            }
            else{
                System.out.println("La temperatura esta per sobre de 45");
            }
            return;
        }
        else if(!(tos.equals("No") || tos.equals("Seca") || tos.equals("Expectorant"))){
            System.out.println("No has posat una de les opcions");
            return;
        }
        else if(presio_max < 80 || presio_max > 180){
            if (presio_max < 80){
                System.out.println("La presio maxima esta per sota de 80");
            }
            else{
                System.out.println("La presio maxima esta per sobre de 180");
            }
            return;
        }


        if (temperatura < 32 || temperatura > 42 || presio_max < 100){
            categoria = 'M';
        }
        else if ((temperatura >= 35 && temperatura <= 38) && tos.equals("No") && (presio_max >= 120 && presio_max <=140)){
            categoria = 'E';
        }
        else if (temperatura > 38 && tos.equals("Expectorant") ){
            categoria = 'D';
        }
        else if (temperatura > 38 && tos.equals("Seca")){
            categoria = 'C';
        }
        else if ((temperatura >= 35 && temperatura <= 38) && tos.equals("No") && (presio_max < 120 || presio_max > 140)){
            categoria = 'B';
        }
        else{
            categoria = 'A';
        }

        if(categoria == 'M'){
            System.out.println("No hi ha res a fer.");
        }

        if(categoria == 'E' || categoria == 'M'){
            System.out.println("Que esperi sentat... o estirat.");
        }
        else if(categoria == 'D'){
            System.out.println("L'ha de visitar el neumòleg d'urgències.");
        }
        else if(categoria == 'C'){
            System.out.println("Envia'l a la planta COVID.");
        }
        else if(categoria == 'B'){
            System.out.println("Ràpid! Que vingui un cardiòleg!");
        }
        else{
            System.out.println("Fes-lo esperar una mica i li passes al Dr House, el de 'casos especials'.");
        }
        
        /////////////////////////////////////////////////////////////////////////////   47
        String codi = "aa";
        String text_si_no, text, escut, forma_llarga;
        float  preu_amplada_alçada, subtotal,preu_forma, preu_text, preu_final, preu_transport;
        char forma;
        int amplada, alçada;
        
        while (!(codi.equals("F1") || codi.equals("K2")  || codi.equals("R1")  || codi.equals("V3")  || codi.equals("M4")  || codi.equals("H5")  || codi.equals("Q6")  || codi.equals("O7") )){
            System.out.println("ESCUT STAR TRECK");
            System.out.println("F1 - Ferengi");
            System.out.println("K2 - Klingon");
            System.out.println("R1 - Romulans");
            System.out.println("V3 - Vulcanians");
            System.out.println("M4 - Madrid");
            System.out.println("H5 - Hydra");
            System.out.println("Q6 - Quentin");
            System.out.println("O7 - Osa");
            System.out.println("ZZZ000 - Tencar el programa");
            System.out.print("Escriu el codi de l’escut que vols: ");

            codi = scan.nextLine();
            codi =  codi.toUpperCase();

            if (codi.equals("ZZZ000")){
                return;
            }
        }
        switch (codi) {
            case "F1":
                escut = "Ferengi";
                break;
            case "K2":
                escut = "Klingon";
                break;
            case "R1":
                escut = "Romulans";
                break;
            case "V3":
                escut = "Vulcanians";
                break;
            case "M4":
                escut = "Madrid";
                break;
            case "H5":
                escut = "Hydra";
                break;
            case "Q6":
                escut = "Quentin";
                break;
            case "O7":
                escut = "Osa";
                break;
            default:
                System.out.println("No has introduit un codi valid");
                return;
        }

        
        System.out.print("\nEscriu amplada i alçada en centímetres, forma (R, A o O), si vols un text (SI o NO) i el text: ");

        amplada = scan.nextInt();
        alçada = scan.nextInt();
        forma = scan.next().charAt(0);
        text_si_no = scan.next();
        text = scan.nextLine();
        
        forma = Character.toUpperCase(forma);
        text_si_no = text_si_no.toUpperCase();
        text = text.trim();


        if (!(forma == 'R' || forma == 'A' || forma == 'O')){
            System.out.println("Error, no has intriduit una forma valida");
            return;
        }
        else if (!(text_si_no.equals("SI") || text_si_no.equals("NO"))){
            System.out.println("Error, no has intriduit un valor valid en l'apartat de decidir si vols o no text");
            return;
        }
        else if ((text_si_no.equals("SI")  && text.length() > 24) || (text_si_no.equals("SI") && text.length() == 0) ){
            if (text.length() > 24 ){
                System.out.println("Error, has introduit un text de més de 24 caracters");
            }
            else{
                System.out.println("Error, no has introduit ningun valor en la variable text");
            }
            return;
        }

        preu_amplada_alçada = (amplada * alçada) / 100;

        if (forma == 'A'){
               
            preu_forma = 1;
            forma_llarga = "Cantonades arrodonides";
        }
        else if (forma == 'O'){
            
            preu_forma = 2;
            forma_llarga = "Cantonades obalades";
        }
        else{
            forma_llarga = "Cantonades rectangulars";
            preu_forma = 0;
        }
        System.out.println(text_si_no);

        if (text_si_no.equals("SI")){
            preu_text = (text.length() * 10) / 100;
        }
        else{
            preu_text = 0;
        }

        subtotal = preu_amplada_alçada + preu_forma + preu_text;

        if (subtotal < 15) {
            preu_transport = 3;
            preu_final = subtotal + preu_transport;
        }
        else{
            preu_transport = 0;
            preu_final = subtotal + preu_transport;
        }

        System.out.printf("\nBandera (o excut) %10s\n", escut);
        System.out.printf("Amplada i alçada %12d X %d = %d cm2 %15.2f €\n",amplada, alçada, amplada * alçada, preu_amplada_alçada );
        System.out.printf("%-27s %-23s %10.2f €\n","Forma", forma_llarga, preu_forma);
        System.out.printf("%-27s %-23s %10.2f €\n","Text", text, preu_text);
        System.out.printf("%-27s %33.2f €\n","Subtota", subtotal);
        System.out.printf("%-27s %33.2f €\n","Transport", preu_transport);
        System.out.printf("%-27s %33.2f €\n","TOTAL",preu_final);
        
        /////////////////////////////////////////////////////////////////////////////   48
        
        // String lletres = "abcdefghijklmnñopqrstuvwxyz";


        // String lletres = "zyxwvutsrqpoñnmlkjihgfedcba";

        // char c = 'a';
        

        
        for (char i = 'a'; i <= 'z'; ++i){

            // c = lletres.charAt(i);

            System.out.println(i);
            
        }
        for (char e = 'z'; e >= 'a'; --e){

            // c = lletres.charAt(i);

            System.out.println(e);
            
        }
        
        /////////////////////////////////////////////////////////////////////////////   49

        final String Contrasenya = "hola1q";
        
        String posar_contrasenya = "a";


        for (int i = 0; i < 3 && !posar_contrasenya.equals(Contrasenya); ++i){
            System.out.print("Contrasenya: ");
            posar_contrasenya = scan.nextLine();
        }

        if (posar_contrasenya.equals(Contrasenya)){
            System.out.println("Has posat la contrasenya be");
        }
        else{
            System.out.println("Has arribat al numero maxim d'intents");
        }
        
        /////////////////////////////////////////////////////////////////////////////   50
        
        List<Integer> llista = new ArrayList<>();
        int numero, nRespostes = 0;
        


        do{
            System.out.print("Introdueix un numero (0 per sortir) ");
            numero = scan.nextInt();
            
            if (numero != 0 || nRespostes == 0){
                llista.add(numero);
            }
            
            ++nRespostes;


        }while (numero != 0);
        

        System.out.println("el numero més gran és " + Collections.max(llista) + " i el numero més petit és " + Collections.min(llista));
        
        
        /////////////////////////////////////////////////////////////////////////////   51
        
        int numero, factorial;
        int i;

        System.out.print("Introdueix un numero entre 1 i 20 ");
        numero = scan.nextInt();
        
        factorial = numero;
        i = numero -1;
        
        
        while (i > 0){

            factorial= factorial * i;
            --i;
        }


        // for(int i = numero - 1; i > 0; --i){
        //     factorial = factorial * i;
        // }
        System.out.println(factorial);

        
        /////////////////////////////////////////////////////////////////////////////   52

        int numero, numero_usuari, intents = 0;

        numero = (int)Math.floor(Math.random()*(100-0+1)+0);
        System.out.println(numero);

        do{
            System.out.println("Introdueix el numero entre 1 i 100 ");
            numero_usuari = scan.nextInt();
            scan.nextLine();
            
            
            ++intents;
            if (numero == numero_usuari){
                System.out.println("Felicitats has acertat, en " + intents + " intents");
            }
            else if(numero > numero_usuari){
                System.out.println("El numero introduit és més petit");
            }
            else{
                System.out.println("El numero introduit és més gran");
            }
            
        }while (numero != numero_usuari);

        
        /////////////////////////////////////////////////////////////////////////////   53
        
        int numero, intercecions = 0;
        


        System.out.println("Intodueix un numeor més gran que 0 ");
        numero = scan.nextInt();
        scan.nextLine();

        
        
        while(numero > 1){
            
            if (numero % 2 == 0){
                numero = numero / 2;
            }
            else{
                numero = (numero * 3) + 1;
            }
            ++intercecions;
            
        }


        // for(int i = 1; numero > 1; i++){
        //     if (numero % 2 == 0){
        //         numero = numero / 2;
        //     }
        //     else{
        //         numero = (numero * 3) + 1;
        //     }
        //     intercecions = i;
        // }

        System.out.println("EL programa a fet " + intercecions + " intercecions");



        
        /////////////////////////////////////////////////////////////////////////////   54

        float nota, mitjana;
        int num_notes = 0;
        float total_notes = 0;


        while(true){
            System.out.println("Introdueix una nota entre 1 i 10 (0 per finalitzar)");
            nota = scan.nextFloat();
            scan.nextLine();

            if (nota == 0 && num_notes != 0){
                mitjana = total_notes / num_notes;

                System.out.println("La mitjana és " + mitjana);
                return;
            }
            else if (nota == 0 && num_notes == 0){
                System.out.println("no posis un 0 com el primer numero ");
                return;
            }
            
            total_notes += nota;


            num_notes += 1;
            
        }
        
        /////////////////////////////////////////////////////////////////////////////   55

        double x,y,hipotenusa;
        double comptador = 0;

        
        for(int i = 0; i != 4000000; ++i){
            x  = Math.random()*2 -1;
            y = Math.random()*2-1;

            hipotenusa = Math.sqrt((y * y) + (x * x));
            if (hipotenusa <= 1){
                ++comptador;
                // comptador += hipotenusa;
            }
        }
        comptador = comptador /1000000;
        System.out.println(comptador);

        
        /////////////////////////////////////////////////////////////////////////////   56

        for(int i = 25;i != 101; i++){

            if (i % 5 == 0 ){
                System.out.println(i);
            }

        }


        
        /////////////////////////////////////////////////////////////////////////////   57

        int numero = 5;
        int numero1 = 2;
        int numero2 = 3;

        System.out.println("Multiples de " + numero + ", " +  numero1 + " i " + numero2);

        for(int i = 1; i != 101; i++){
            
            
            if(i % numero == 0 && i % numero1 == 0 && i % numero2 == 0){
                System.out.println(i);
            }
        }

        
        /////////////////////////////////////////////////////////////////////////////   58

        int numero;

        System.out.print("Introdueix un numero: ");
        numero = scan.nextInt();
        scan.nextLine();

        for(int i = 0; i != 11; i++){
            System.out.println(numero + " * " + i  + " = " + numero * i);
        }

        
        /////////////////////////////////////////////////////////////////////////////   59

        

        int max_aleatori, min_aleatori, max_intents;
        int numero, numero_usuari = 0, numero_intents = 0;
        String continuar = "no";

        System.out.print("Quins numeros son el maxim i el minim en el que pot estar el numero, i el numero maxim d'intents: ");
        max_aleatori = scan.nextInt();
        min_aleatori = scan.nextInt();
        max_intents = scan.nextInt();
        scan.nextLine();
        

        do{
            numero = (int)Math.floor(Math.random()*(max_aleatori-min_aleatori+1)+0);
            System.out.println(numero);
            
            
            
            for(int i = 1; i != (max_intents + 1) &&  numero != numero_usuari; i++ ){
                System.out.println("Introdueix el numero entre 1 i 100 ");
                numero_usuari = scan.nextInt();
                scan.nextLine();
                
                
                
                if (numero > numero_usuari){
                    System.out.println("El numero introduit és més petit");
                }
                else if(numero < numero_usuari){
                    System.out.println("El numero introduit és més gran");
                }
                else{
                    System.out.println("Has encertat, vols tornar a probar? (si, no) ");
                    continuar = scan.nextLine();
                    break;
                }

                if (i == max_intents){
                    System.out.println("Ja no tens més intents, vols tornar a probar? (si, no) ");
                    continuar = scan.nextLine();
                }
                numero_intents = i;
            }   
        } while (continuar.equals("si"));


        if(numero == numero_usuari){
            System.out.println("Felicitats has acertat, en " + numero_intents + " intents");
        }
        else{
            System.out.println("Has superat el numero màxim d'intents");
        }
        
        
        /////////////////////////////////////////////////////////////////////////////   60

        int numero;

        System.out.println("introdueix un numero enter entre 1 i 80: ");
        numero = scan.nextInt();
        scan.nextLine();

        for(int i = 0; i != numero; i++){
            System.out.print("*");
        }

        
        /////////////////////////////////////////////////////////////////////////////   61
        


        int numero, contador = 1;

        System.out.println("introdueix un numero enter entre 1 i 80: ");
        numero = scan.nextInt();
        scan.nextLine();

        for(int i = 0; i != numero; i++){
            
            for(int a = 0; a<i; a++){
                System.out.print("*");
            }
            System.out.println("*");
            
            // if(contador == 1 || contador == 3){
            //     System.out.println("*");
            // }
            // else{
            //     System.out.print("*");
            // }
            // contador++;
            // if(contador == 4){
            //     contador = 1;
            // }
        }


        
        /////////////////////////////////////////////////////////////////////////////   62

        for(int i = 10; i != 0; i--){
            System.out.println(i);
            Thread.sleep(1000);
        }

        
        /////////////////////////////////////////////////////////////////////////////   63


        float y;

        for(float x = -5; x != 5.5; x += 0.5){
            y = (float)Math.pow(((x - 2 ) / 5),2) + 4;
            System.out.printf("(%.1f|%.1f) ",x,y);
        }

        
        /////////////////////////////////////////////////////////////////////////////   64

        int num1 = 12, num2 = 20, num3 = 8,num_mes_gran;
        char caracter1 = '#';
        char caracter2 = '0';
        char caracter3 = 'X';

        if (num1 >= num2 && num1 >= num3){
            num_mes_gran = num1;
        }
        else if(num2 > num1 && num2 >= num3){
            num_mes_gran = num2;
        }
        else{
            num_mes_gran = num3;
        }


        for(int i  = 0; i != num_mes_gran; i++){
            if(num1 == i){
                caracter1 = ' ';
            }
            if(num2 == i){
                caracter2 = ' ';
            }
            if(num3 == i){
                caracter3 = ' ';
            }
            System.out.printf("%c%c%c%c%c%c%c%c%c%c%c%c\n",caracter1,caracter1,caracter1,caracter1,caracter2,caracter2,caracter2,caracter2,caracter3,caracter3,caracter3,caracter3);
        }
        
        
        
        // for(int i = 0; i != num1; i++){
        //     if(i == num1 -1){
        //         System.out.println("#");
        //         for(int a = 0; a != num1; a++){
        //             if(a == num1 -1){
        //                 System.out.println("#");
        //             }
        //             else{
        //                 System.out.print("#");
        //             }
        //         }
        //     }
        //     else{
        //         System.out.print("#");
        //     }
            
        // }
        
        // for(int i = 0; i != num2; i++){
        //     if(i == num2 -1){
        //         System.out.println("0");
        //         for(int a = 0; a != num2; a++){
        //             if(a == num2 -1){
        //                 System.out.println("0");
        //             }
        //             else{
        //                 System.out.print("0");
        //             }
        //         }
        //     }
        //     else{
        //         System.out.print("0");
        //     }
            
        // }

        // for(int i = 0; i != num3; i++){
        //     if(i == num3 -1){
        //         System.out.println("X");
        //         for(int a = 0; a != num3; a++){
        //             if(a == num3 -1){
        //                 System.out.println("X");
        //             }
        //             else{
        //                 System.out.print("X");
        //             }
        //         }
        //     }
        //     else{
        //         System.out.print("X");
        //     }
            
        // }
        


        
        /////////////////////////////////////////////////////////////////////////////   65
        
        

        int num1 = 12, num2 = 20, num3 = 8,num_mes_gran;
        char caracter1 = ' ';
        char caracter2 = ' ';
        char caracter3 = ' ';

        if (num1 >= num2 && num1 >= num3){
            num_mes_gran = num1;
        }
        else if(num2 > num1 && num2 >= num3){
            num_mes_gran = num2;
        }
        else{
            num_mes_gran = num3;
        }


        for(int i  = 0; i != num_mes_gran; i++){
            if(num_mes_gran - num1 == i){
                caracter1 = '#';
            }
            if(num_mes_gran - num2 == i){
                caracter2 = '0';
            }
            if(num_mes_gran - num3 == i){
                caracter3 = 'X';
            }
            System.out.printf("%c%c%c%c%c%c%c%c%c%c%c%c\n",caracter1,caracter1,caracter1,caracter1,caracter2,caracter2,caracter2,caracter2,caracter3,caracter3,caracter3,caracter3);
        }
        
        
        /////////////////////////////////////////////////////////////////////////////   66
        
        for(int i = 1; i != 11; i++){
            for(int a = 1; a != 11; a++){
                if(a != 10){
                    System.out.printf("%4d",a*i);
                }
                else{
                    System.out.printf("%4d\n",a*i);
                }
                
            }
        }
        
        
        /////////////////////////////////////////////////////////////////////////////   67

        double hipotenusa;
        double comptador = 0;
        double increment = 0.0001;
        
        
        for(double x = -1; x < 1; x += increment){
            for(double y = -1; y < 1; y += increment){
                hipotenusa = Math.sqrt((y * y) + (x * x));
                if (hipotenusa <= 1){
                    ++comptador;
                    
                }
            }
        }
            
        
        comptador = comptador / 100000000;
        System.out.println(comptador);

        
        /////////////////////////////////////////////////////////////////////////////   68

        String hola = "Hello World!";

        for(int i = 1; i < (hola.length() + 1); i++){
            
            char a = hola.charAt(hola.length() - i);
            System.out.print(a);
        }

        
        /////////////////////////////////////////////////////////////////////////////   69

        String hola = "Hi havia una vegada ";

        for(int i = 0; i < hola.length(); i++){
            
            char a = hola.charAt(i);
            if ( a == ' '){
                System.out.println(i);
            }
            
        }
        
        
        /////////////////////////////////////////////////////////////////////////////   70

        String hola = "Administració de sistemes informàtics en xarxa";

        // hola = hola.replace("de ", "");
        // hola = hola.replace("en ", "");
        if(hola.indexOf(" ") - 0 > 3){
            System.out.print(hola.charAt(0));
        }
    
        
        for(int i = 0; i < hola.length(); i++){
            char a = hola.charAt(i);
            
            
            if(hola.charAt(i) == ' '){
                
                if(hola.indexOf(" ", i +1) == -1){
                    if(hola.length() - (hola.indexOf(" ", i) + 1) > 3){
                        a = hola.charAt(i + 1);
                        a = Character.toUpperCase(a);
                        System.out.print(a);
                    }
                    
                }
                else{
                    if(hola.indexOf(" ", i + 1) - 1 - hola.indexOf(" ", i ) > 3){
                        a = hola.charAt(i + 1);
                        a = Character.toUpperCase(a);
                        System.out.print(a);
                    }
                    
                }
                
            }
        }

         
        /////////////////////////////////////////////////////////////////////////////   71

        String hola = "Hi havia una vegada    as";
        String copia = "";
        // hola = hola.replace("  ", " ");
        // hola = hola.replace(" ", "|");
        System.out.println(hola);
        for(int i = 0; i < hola.length() ; i++){
            if(hola.charAt(i) != ' '){
                copia += hola.charAt(i);                
            }
            else{
                if (!(hola.charAt(i - 1) == ' ')){
                    copia += '|';
                }
                
                
            }
            
        }
        System.out.println(copia);

        
        /////////////////////////////////////////////////////////////////////////////   72


        String hola = "a@";
        boolean error = true;

        if (hola.contains("#")){
            System.out.println("Conte el caracter #");
            error = false;
        }
        if (hola.contains("@")){
            System.out.println("Conte el caracter @");
            error = false;
        }
        if (hola.contains("€")){
            System.out.println("Conte el caracter €");
            error = false;
        }
        if (error){
            System.out.println("No conte cap caracter especial");
        }


        
        /////////////////////////////////////////////////////////////////////////////   73

        String hola1 = "ddhahdja";
        String hola2 = "edhahdja";

        if(hola1.compareToIgnoreCase(hola2) == 0){
            System.out.print("Son iguals");
        }
        else if(hola1.compareToIgnoreCase(hola2) > 0){
            System.out.print("El segon string hola2 és més gran");
        }
        else{
            System.out.print("El primer string hola1 és més gran");
        }

        if(hola1.length() == hola2.length()){
            System.out.println(", i tenen el mateix nombre de caracters");
        }
        else if(hola1.length() > hola2.length()){
            System.out.println(", i hola1 te més caracters");
        }
        else{
            System.out.println(", i hola2 te més caracters");
        }


        
        /////////////////////////////////////////////////////////////////////////////   74

        String hola = "Administració de sistemes informàtics en xarxa";
        

        
        
        System.out.println(hola.substring(0, hola.indexOf(" ")));
        
        
        for(int i = 0; i< hola.length(); i++){
            if(hola.charAt(i) == ' '){
                if(hola.indexOf(" ", i +1) == -1){
                    System.out.println(hola.substring(hola.indexOf(" ", i) + 1));
                }
                else{
                    System.out.println(hola.substring(hola.indexOf(" ", i) + 1, hola.indexOf(" ", i +1)));
                }
                
            }         
        }
        
        /////////////////////////////////////////////////////////////////////////////   74.2
        
        String hola = "Administració de sistemes informàtics en xarxa";
        String p1 = "";


        int ndx = hola.indexOf(" ");
        while (ndx >= 0){
            p1 = hola.substring(0, ndx);
            System.out.println(p1);
            hola = hola.substring(ndx + 1);
            ndx = hola.indexOf(" ");

        }
        System.out.println(hola);

        
        /////////////////////////////////////////////////////////////////////////////   75

        String nom, cognom;

        System.out.print("Escriu el teu nom i cognom: ");
        nom = scan.next();
        cognom = scan.nextLine();


        cognom = cognom.trim().toLowerCase();
        

        System.out.println(nom.toLowerCase().charAt(0) + "." + cognom + "@sapalomera.cat");
    

        
        /////////////////////////////////////////////////////////////////////////////   76


        String hola = "CoChE";
        String hola1 = "EsTaDiO";
        String falten = "";
        String falten1 = "";
        
        hola = hola.toLowerCase();
        hola1 = hola1.toLowerCase();

        if(hola.contains("a")  && hola.contains("e") && hola.contains("i") && hola.contains("o") && hola.contains("u") ){
            System.out.println("Conte les lletres");
        }
        else{
            if(!(hola.contains("a"))) falten += "A";
            if(!(hola.contains("e"))) falten += "E";
            if(!(hola.contains("i"))) falten += "I";
            if(!(hola.contains("o"))) falten += "O";
            if(!(hola.contains("u"))) falten += "U";

            
            System.out.println(hola + " -> Falten vocals: " + falten);
        }
        
        if(hola1.contains("a")  && hola1.contains("e") && hola1.contains("i") && hola1.contains("o") && hola1.contains("u") ){
            System.out.println("Conte les lletres");
        }
        else{
            if(!(hola1.contains("a"))) falten1 += "A";
            if(!(hola1.contains("e"))) falten1 += "E";
            if(!(hola1.contains("i"))) falten1 += "I";
            if(!(hola1.contains("o"))) falten1 += "O";
            if(!(hola1.contains("u"))) falten1 += "U";

            
            System.out.println(hola1 + " -> Falten vocals: " + falten1);
        }



        
        /////////////////////////////////////////////////////////////////////////////   77


        String hola = "HOLA";


        if(hola.toLowerCase().equals(hola)){
            System.out.println("Minuscules");
        }
        else if (hola.toUpperCase().equals(hola)){
            System.out.println("Majuscules");
        }
        else{
            System.out.println("Mixte");
        }

        
        /////////////////////////////////////////////////////////////////////////////   78


        String s1 = "ABC";
        String s2 = "DEF";
        String s3 =  "AB" + "C";
        String s4 = "AbC";
        String s5 =  "ABC";

        
        System.out.println(s1.compareTo(s2));
        System.out.println(s2.equals(s3));
        System.out.println(s1.equalsIgnoreCase(s4));
        System.out.println(s3 == s1);
        System.out.println(s3 == s5);
        System.out.println(s2.compareTo(s3));
        System.out.println(s3.equals(s1));


        
        /////////////////////////////////////////////////////////////////////////////   79

        String hola = "Afhbijznlfu";
        char lletra_gran = 'A';
        char lletra_petita = '~';
        
        hola = hola.toLowerCase();
        for(int i = 0; i < 10; i++){
            
            if(hola.charAt(i) < lletra_petita){
                lletra_petita = hola.charAt(i);
            }

            if(hola.charAt(i) > lletra_gran){
                lletra_gran = hola.charAt(i);
            }

        }
        System.out.println(lletra_gran + " és la lletra més gran");
        System.out.println(lletra_petita + " és la lletra més petita");


        
        /////////////////////////////////////////////////////////////////////////////   80

        int l = 1234554321;

        String num = Integer.toString(l);
        Boolean capicua = true;

        
        for(int i = 0; capicua == true && i < num.length(); i++){
            if (num.charAt(i) != num.charAt((num.length() - 1) - i)){
                System.out.println("No és cap i cua");
                capicua = false;
                
            }
        }
        if(capicua == true){
            System.out.println("és capicua");
        }

        
        /////////////////////////////////////////////////////////////////////////////   81

        String base = "Adivina ya te opina, ya ni miles origina, ya ni cetro me domina, ya ni monarcas, a repaso ni mulato carreta, acaso nicotina, ya ni cita vecino, anima cocina, pedazo gallina, cedazo terso nos retoza de canilla goza, de pánico camina, ónice vaticina, ya ni tocino saca, a terracota luminosa pera, sacra nómina y ánimo de mortecina, ya ni giros elimina, ya ni poeta, ya ni vida";
        base = Normalizer.normalize(base, Normalizer.Form.NFD);
        Boolean capicua = true;
        String num = "";
        base = base.toLowerCase();
        
        for(int i = 0; i < base.length(); i++){
            if(base.charAt(i) >= 97 && base.charAt(i) <= 122){
                num += base.charAt(i);
            }
        }
        
        for(int a = 0; capicua == true && a < num.length(); a++){
            if (num.charAt(a) != num.charAt((num.length() - 1) - a)){
                System.out.println("No és capicua");
                capicua = false;
                
            }
        }

        if(capicua == true){
            System.out.println("és capicua");
        }
         
        /////////////////////////////////////////////////////////////////////////////   82

        float[] notes = new float[6];
        float notamax = 0;
        float notamin = 10;
        boolean Suspes = false;
        int i;
        float mitjana = 0;


        System.out.println("posa les notes de les 6 ufs");
        notes[0] = scan.nextFloat();
        notes[1] = scan.nextFloat();
        notes[2] = scan.nextFloat();
        notes[3] = scan.nextFloat();
        notes[4] = scan.nextFloat();
        notes[5] = scan.nextFloat();
        scan.nextLine();

        
        for (i = 0; i < notes.length; i++){
            if (notes[i] > notamax){
                notamax = notes[i];
            }
            if (notes[i] < notamin){
                notamin = notes[i];
                if (notamin < 5){
                    Suspes  = true;
                    
                }
            }
        }

        for (i = 0; i < notes.length && !Suspes; i++){

            mitjana += notes[i];

        }

        if (Suspes){
            System.out.println("La nota maxima és " + notamax);
            System.out.println("La nota més petita és " + notamin + " i com és més petit que 5 té el modul suspes");
        }
        else{
            System.out.println("La nota maxima és " + notamax);
            System.out.println("La nota més petita és " + notamin);
            System.out.println("La mitjana és " + mitjana/6);
        }

        
        /////////////////////////////////////////////////////////////////////////////   83


        float[] notes = new float[10];
        int i;
        int suspes = 0, aprovat = 0;
        float mitjana = 0;
        

        for(i = 0; i < notes.length; i++){
            System.out.println("Nota: ");
            notes[i] = scan.nextFloat();
            scan.nextLine();
        }

        for(i = 0; i < notes.length; i++){
            if(notes[i] < 5){
                suspes++;
            }
            else{
                aprovat++;
                mitjana += notes[i];
            }
            
        }
        System.out.println("EL nombre de suspesos és " + suspes);
        System.out.println("EL nombre de aprovats és " + aprovat);
        System.out.println("Les notes dels aprobats són:");
        for(i = 0; i < notes.length; i++){
            if(notes[i] >= 5){
                System.out.println(notes[i]);
            }
        }
        System.out.println("La mitjana dels aprovats és " + mitjana/aprovat);

        
        /////////////////////////////////////////////////////////////////////////////   84

        int[] aleatori = new int[1000];
        int aleatori_parell = 0;

        for (int i = 0; i < aleatori.length; i++){
            aleatori[i] = (int)Math.floor(Math.random()*(1000-100+1)+100);
        }
        for (int a = 0; a < aleatori.length && aleatori_parell < 10; a++){
            if(aleatori[a] % 2 == 0){
                System.out.println(aleatori[a]);
                aleatori_parell++;
            }
        }

        
        /////////////////////////////////////////////////////////////////////////////   85

        int[] aleatori1 = new int[10];
        int[] aleatori2 = new int[10];
        int i, suma = 0, multiplicacio = 0;



        for (i = 0; i < 10; i++){
            aleatori1[i] = (int)Math.floor(Math.random()*(100-1+1)+1);
        }
        for (i = 0; i < 10; i++){
            aleatori2[i] = (int)Math.floor(Math.random()*(100-1+1)+1);
        }
        System.out.println("Multiplicació: ");
        for(i = 0; i < aleatori1.length; i++){
            multiplicacio = aleatori1[i] * aleatori2[i];
            System.out.println(aleatori1[i] +  " * " + aleatori2[i] + " = " + multiplicacio);
        }
        System.out.println("Suma: ");
        for(i = 0; i < aleatori1.length; i++){            
            for(int a = 0; a < aleatori2.length; a++){
                suma = aleatori1[i] + aleatori2[a];
                if(a == aleatori2.length - 1){
                    System.out.println(suma);
                }
                else{
                    System.out.print(suma + " ");
                }
                
            }
            
        }

        
        /////////////////////////////////////////////////////////////////////////////   86

        char[] lletra = new char[10];
        int i;
        Random r;

        System.out.println("Lletres: ");
        for(i = 0; i < lletra.length; i++){
            r = new Random();
            lletra[i] = (char)(r.nextInt(26) + 'a');
            System.out.println(lletra[i]);
        }
        System.out.println("Lletres inverses: ");
        for (i  = lletra.length - 1; i >= 0; i--){
            System.out.println(lletra[i]);
        }
        
        
        /////////////////////////////////////////////////////////////////////////////   87
        
        
        int[] llista_numeros = {1, 2, 3, 4};
        int i;
        boolean mes_gran = false;
        boolean mes_petit = false;
        boolean igual = false;

        for(i = 0; i < llista_numeros.length; i++){
            
            if(i == llista_numeros.length - 1){
                if(mes_gran){
                    System.out.println("Ordenat descendent");
                }
                else if(mes_petit){
                    System.out.println("Ordenat ascendent");
                }
                else{
                    System.out.println("Tots són iguals");
                }
            }
            else{
                if(llista_numeros[i] > llista_numeros[i + 1] && (i == 0 || igual) ){
                    mes_gran = true;
                }
                else if(llista_numeros[i] < llista_numeros[i + 1] && (i == 0 || igual)){
                    mes_petit = true;
                }
                else if(llista_numeros[i] == llista_numeros[0]){
                    igual = true;
                }
                
                if(llista_numeros[i] > llista_numeros[i + 1] && mes_petit){
                    
                    System.out.println("No ordenat");
                    break;
                    
                    
                }
                else if(llista_numeros[i] < llista_numeros[i + 1] && mes_gran){
                    
                    System.out.println("No ordenat");
                    break;
                    
                    
                }
            }
        }
        
        /////////////////////////////////////////////////////////////////////////////   88
        
        int[] llista1 = {1, 1, 1,1};
        int[] llista2 = {2, 2, 2, 2, 2};
        int[] llista_nova = new int[llista1.length + llista2.length];
        int contador_llista1 = 0;
        int contador_llista2 = 0;

        for(int i = 0; i < llista_nova.length; i++){
            if((i % 2 == 0 || contador_llista2 == llista2.length) && contador_llista1 < llista1.length){
                llista_nova[i] = llista1[contador_llista1];
                contador_llista1++;
            }
            else if((i % 2 != 0 || contador_llista1 == llista1.length) && contador_llista2 < llista2.length){
                llista_nova[i] = llista2[contador_llista2];
                contador_llista2++;
            }
            System.out.print(llista_nova[i]);
        }

        
        /////////////////////////////////////////////////////////////////////////////   89
        
        int[] llista_aleatoria = new int[10];
        int i;
        for(i = 0; i < llista_aleatoria.length; i++){
            llista_aleatoria[i] = (int)Math.floor(Math.random()*(99-0+1)+0);
        }
        for(i = 0; i < llista_aleatoria.length -1; i++){
            System.out.println(llista_aleatoria[i] + "  + " + llista_aleatoria[i + 1] + " = " + (llista_aleatoria[i] + llista_aleatoria[i + 1]));
        }

        
        /////////////////////////////////////////////////////////////////////////////   90
        
        char[] lletra = new char[10];
        char[] lletra_desplacada = new char[lletra.length];
        int canvi = 1;
        int canvi_estatic = canvi;
        int contador_final = 0;
        int i;
        Random r;

        System.out.println("Lletres: ");
        for(i = 0; i < lletra.length; i++){
            r = new Random();
            lletra[i] = (char)(r.nextInt(26) + 'a');
            System.out.print(lletra[i]);
        }
        System.out.println();
        System.out.println("Lletres desplaçades");
        for(i = 0; i < lletra.length; i++){
            if(i +1 > lletra.length - canvi_estatic){
                lletra_desplacada[contador_final] = lletra[i];
                contador_final++;
            }
            else{
                lletra_desplacada[canvi] = lletra[i];
                canvi++;
            }
            
        }
        System.out.print(lletra_desplacada);



        
        /////////////////////////////////////////////////////////////////////////////   91
        
        int[] llista_aleatoria = new int[100];
        int i;
        int num_usuari = 10;
        int contador = 0;
        int[] posicio = new int[100];

        for(i = 0; i < llista_aleatoria.length; i++){
            llista_aleatoria[i] = (int)Math.floor(Math.random()*(999-0+1)+0);
        }
        for(i = 0; i < llista_aleatoria.length; i++){
            if(llista_aleatoria[i] == num_usuari){
                
                posicio[contador] = i + 1;
                contador++;
            }
        }
        System.out.println("Hi han " + contador + " " + num_usuari);
        System.out.println("Ha aparegut en les posicions:");
        for(i = 0; posicio[i] != 0;i++){
            System.out.println(posicio[i]);
        }

        
        /////////////////////////////////////////////////////////////////////////////   92
        

        int[] llista_aleatoria1 = new int[100];
        int[] llista_aleatoria2 = new int[100];
        int i;
        int[] posicio = new int[100];

        for(i = 0; i < llista_aleatoria1.length; i++){
            llista_aleatoria1[i] = (int)Math.floor(Math.random()*(9-0+1)+0);
        }
        for(i = 0; i < llista_aleatoria2.length; i++){
            llista_aleatoria2[i] = (int)Math.floor(Math.random()*(9-0+1)+0);
        }
        for(i = 0; i < llista_aleatoria1.length; i++){
            if(llista_aleatoria1[i] == llista_aleatoria2[i]){
                System.out.println("Coincideixen en la posició " + (i+1) + " i el numero que coincideix és " + llista_aleatoria1[i]);
            }
        }
        
        /////////////////////////////////////////////////////////////////////////////   93
        

        char[] lletra = new char[10];       
        int i;
        Random r;
        char ascii  = 97;
        int contador;

        System.out.println("Lletres: ");
        for(i = 0; i < lletra.length; i++){
            r = new Random();
            lletra[i] = (char)(r.nextInt(26) + 'a');
            System.out.print(lletra[i]);
        }
        System.out.println();
        for(contador = 0; ascii < 123; ascii++){
            for(i = 0; i < lletra.length; i++){
                if(ascii == lletra[i]){
                    contador++;
                }
            }
            System.out.println("Hi han " + contador + " lletres " + ascii);
            contador = 0;
        }

        
        /////////////////////////////////////////////////////////////////////////////   94
        
        String frase = "Hola que, tal com estas";
        String[] separat = new String[frase.length()];
        separat = frase.trim().split(" +"); // EL + serbeix per buscar un o més espais

        for(int i = separat.length - 1; i >= 0; i--){
            System.out.print(separat[i] + " ");
            
        }

        
        /////////////////////////////////////////////////////////////////////////////   95
        
        String frase = "zola que tal com estas";
        frase = frase.toLowerCase();
        char sobra;
        char[] separat = new char[frase.length()];
        separat = frase.trim().toCharArray();

        for(int i = 0; i < separat.length; i++){
            
            if(separat[i] <= 122 && separat[i] >= 97){
                separat[i] += 1;
                if(separat[i] > 122){
                    sobra = (char)(separat[i] - 122);
                    separat[i] = (char)(sobra + 96);
                    // separat[i] -= 122 - 97 + 1;
                }
            }
        }

        // frase = String.valueOf(separat);
        String f = new String(separat);
        System.out.println(f);


        
        /////////////////////////////////////////////////////////////////////////////   96
        
        final int F = 6;
        final int C = 4;
        int notes[][] = new int[F][C];
        int i;
        int a;
        float mitjana_a = 0;
        float mitjana_uf = 0;
        for(i = 0; i < F; i++){
            for(a = 0; a < C; a++){
                notes[i][a] = (int)Math.floor(Math.random()*(10-0+1)+0);
            }
        }
        System.out.printf("%13s %s %s %s\n", "UF1", "UF2", "UF3", "UF4");
        for(i = 0; i < F; i++){
            System.out.printf("Alumne %d ", i + 1);
            for(a = 0; a < C; a++){
                if(a + 1 == C){
                    System.out.printf("%3d\n", notes[i][a]);
                }
                else{
                    System.out.printf("%3d ", notes[i][a]);
                }
                
            }
            
        }
        System.out.printf("%16s\n", "Mitjana");
        for(i = 0; i < F; i++, mitjana_a = 0){
            for(a = 0; a < C; a++){
                mitjana_a += notes[i][a];
            }
            mitjana_a /= C;
            System.out.printf("Alumne %d %4.1f\n", i+ 1, mitjana_a);
        }
        System.out.printf("%10s\n", "Mitjana");
        for(a = 0; a< C; a++, mitjana_uf= 0){
            for(i = 0; i < F; i++){
                mitjana_uf += notes[i][a];
                
            }
            System.out.printf("UF%d %4.1f\n", a+ 1, mitjana_uf / F);
        }

        
        /////////////////////////////////////////////////////////////////////////////   97
        */
        int f = 5;
        int c = 3;
        String[] dies = {"Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres"};
        String[] hobbies = {"TV", "Música", "Videojocs", "Puzzle"};
        String[] menjars = {"Xococrispis", "Macarrons", "Sopa", "Pizza", "Yatebebo", "Snacks"};
        String[][] agenda = new String[f][c];
        Random random_h;
        Random random_m;
        System.out.printf("%20s %20s %20s\n", "Esmorzar", "Dinar", "Sopar");
        for(int i = 0; i < f; i++){
            System.out.printf("%-12s", dies[i]);
            for(int a = 0; a < c; a++){
                random_h = new Random();
                random_m = new Random();
                agenda[i][a] = hobbies[(int)random_h.nextInt(3)] + " | " + menjars[(int)random_m.nextInt(5)];
                if(a + 1 == c){
                    System.out.printf("%s\n", agenda[i][a]);
                }
                else{
                    System.out.printf("%-21s", agenda[i][a]);
                }
            }
        }

        /*
        /////////////////////////////////////////////////////////////////////////////   98

        int n[] = {9,4,6,3};
        int t;

        // Agafa el primer valor entre el primer i el penúltim
        for (int i = 0; i < n.length - 1; i++) {
        // Agafa el segon valor entre el següent al primer i l'últim
        for (int j = i + 1; j < n.length; j++) {
            // Intercanviar si el primer és més gran que el segon
            if (n[i] > n[j]) {
            t = n[i];
            n[i] = n[j];
            n[j] = t;
            }
        }
        }

        // Mostrar els valors ja ordenats: 3, 4, 6 i 9
        for (int i = 0; i < n.length; i++) {
        System.out.println(n[i]);
        }        
        
        /////////////////////////////////////////////////////////////////////////////   99
        int n[] = {3,4,6,9};      // Llista de valors
        int valor = 6;            // Valor buscat
        int min = 0;              // Primera posició de la llista
        int max = n.length - 1;   // Última posició de la llista
        int mid = 0;              // Posició central (inicialitzar per evitar error)

        while (min <= max) {
        mid = (min + max) / 2;  // Calcular la posició central del tram entre min i max
        if (n[mid] < valor) {
            min = mid + 1;        // Agafar la meitat superior
        } else if (n[mid] > valor) {
            max = mid - 1;        // Agafar la meitat inferior
        } else {
            break;                // Valor trobat
        }
        }

        if (n.length > 0 && n[mid] == valor) {
        System.out.println("S'ha trobat el valor en la posició " + mid);
        } else {
        System.out.println("NO s'ha trobat el valor en la llista");
        }

        
        /////////////////////////////////////////////////////////////////////////////   100
        int n_paraules = 10;
        String[] paraules = new String[n_paraules];
        int i;
        String t;
        String paraula;
        int min = 0;
        int max = paraules.length -1;
        int mid = 0;

        for(i = 0; i < paraules.length; i++){
            System.out.println("Escriu la paraula " + (i+1) + ": ");
            paraules[i] = scan.nextLine();
            paraules[i] = paraules[i].toLowerCase();
        }
        
        for (i = 0; i < paraules.length - 1; i++) {
            // Agafa el segon valor entre el següent al primer i l'últim
            for (int j = i + 1; j < paraules.length; j++) {
                // Intercanviar si el primer és més gran que el segon
                if (paraules[i].compareToIgnoreCase(paraules[j]) > 0){
                    t = paraules[i];
                    paraules[i] = paraules[j];
                    paraules[j] = t;
                }
            }
        }
        System.out.println();
        for(i = 0 ;i < paraules.length; i++){
            System.out.println(paraules[i]);
        }

        do{
            System.out.println("Escriu la paraula que vols trobar(fi per sortir): ");
            paraula = scan.nextLine().toLowerCase();
            if(paraula.equals("fi")){
                break;
            }
            while(min <= max){
                
                mid = (min + max) / 2;
                if(paraules[mid].compareToIgnoreCase(paraula) < 0){
                    min = mid + 1;
                }
                else if(paraules[mid].compareToIgnoreCase(paraula) > 0){
                    max = mid -1;
                }
                else{
                    break;
                }
            }
            if(paraules.length > 0 && paraules[mid].equals(paraula)){
                System.out.println("S'ha trobat el valor en la posició " + mid);
            }
            else{
                System.out.println("No s'ha trobat");
            }
            min = 0;
            max = paraules.length -1;
            mid = 0;

        }
        while(true);
        
        */
        /////////////////////////////////////////////////////////////////////////////   101
        scan.close();
    }
}
